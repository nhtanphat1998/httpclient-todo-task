﻿using Newtonsoft.Json;

namespace TodoTask.Logic
{
    public class TodoManager
    {
        // TODO: Tạo ra mock api bằng cách truy cập https://mockapi.io/
        private const string ApiBaseUrl = "https://5fceec5c9fe7590016e74062.mockapi.io/";

        private static HttpClient httpClient = new HttpClient();

        public async Task<List<Todo>> GetTodos()
        {
            var response = await httpClient.GetAsync($"{ApiBaseUrl}/todos");
            if (response.IsSuccessStatusCode)
            {
                string todosJson = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<Todo>>(todosJson);
            }
            return new List<Todo>();
        }

        public async Task<Todo> GetTodoById(string todoId)
        {
            var response = await httpClient.GetAsync($"{ApiBaseUrl}/todos/{todoId}");
            // TODO: Kiểm tra status code nếu thành công thì tìm thấy còn không thì throw exception
            throw new NotImplementedException();
        }

        public async Task AddTodo(Todo todo)
        {
            // TODO: thực hiện http request lên server kèm đối được todo
            throw new NotImplementedException();
        }

        public async Task UpdateTodo(Todo todo)
        {
            // TODO: thực hiện http request lên server update todo
            throw new NotImplementedException();
        }

        public async Task DeleteTodo(string todoId)
        {
            // TODO: thực hiện http request lên server để xóa todo
            throw new NotImplementedException();
        }
    }
}
