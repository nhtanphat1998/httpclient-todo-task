﻿using TodoTask.Logic;

namespace TodoTask.UI
{
    public class TodoManagerUI
    {
        private readonly TodoManager todoManager;

        public TodoManagerUI()
        {
            todoManager = new TodoManager();
        }

        public async Task Run()
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            while (true)
            {
                Console.WriteLine("------ Quản lý TODOs ------");
                Console.WriteLine("1. Hiển thị danh sách TODOs");
                Console.WriteLine("2. Thêm mới TODO");
                Console.WriteLine("3. Xoá TODO");
                Console.WriteLine("4. Đánh dấu TODO đã hoàn thành");
                Console.WriteLine("5. Chỉnh sửa TODO");
                Console.WriteLine("6. Thoát");

                Console.Write("Lựa chọn của bạn: ");
                string choice = Console.ReadLine();

                switch (choice)
                {
                    case "1":
                        await DisplayTodos();
                        break;

                    case "2":
                        await AddTodo();
                        break;

                    case "3":
                        await DeleteTodo();
                        break;

                    case "4":
                        await MarkAsCompleted();
                        break;

                    case "5":
                        await EditTodo();
                        break;

                    case "6":
                        Console.WriteLine("Tạm biệt!");
                        return;

                    default:
                        Console.WriteLine("Lựa chọn không hợp lệ. Vui lòng chọn lại.");
                        break;
                }
            }
        }

        private async Task DisplayTodos()
        {
            Console.WriteLine("------ Danh sách TODOs ------");
            var todos = await todoManager.GetTodos();

            foreach (var todo in todos)
            {
                Console.WriteLine($"Id: {todo.Id}, Tiêu đề: {todo.Title}, Mô tả: {todo.Description}, Hoàn thành: {todo.IsCompleted}");
            }
        }

        private async Task AddTodo()
        {
            Console.WriteLine("------ Thêm mới TODO ------");
            Console.Write("Tiêu đề: ");
            string title = Console.ReadLine();
            Console.Write("Mô tả: ");
            string description = Console.ReadLine();

            var newTodo = new Todo { Title = title, Description = description, IsCompleted = false };
            await todoManager.AddTodo(newTodo);
            Console.WriteLine("TODO đã được thêm mới thành công.");
        }

        private async Task DeleteTodo()
        {
            Console.WriteLine("------ Xoá TODO ------");
            Console.Write("Nhập Id của TODO cần xoá: ");
            string todoId = Console.ReadLine();

            var todoToDelete = await todoManager.GetTodoById(todoId);

            if (todoToDelete != null)
            {
                await todoManager.DeleteTodo(todoId);
                Console.WriteLine($"TODO có Id {todoId} đã được xoá thành công.");
            }
            else
            {
                Console.WriteLine($"Không tìm thấy TODO với Id {todoId}.");
            }
        }

        private async Task MarkAsCompleted()
        {
            Console.WriteLine("------ Đánh dấu TODO đã hoàn thành ------");
            Console.Write("Nhập Id của TODO cần đánh dấu: ");
            string todoId = Console.ReadLine();

            var todoToMark = await todoManager.GetTodoById(todoId);

            if (todoToMark != null)
            {
                todoToMark.IsCompleted = true;
                await todoManager.UpdateTodo(todoToMark);
                Console.WriteLine($"TODO có Id {todoId} đã được đánh dấu là đã hoàn thành.");
            }
            else
            {
                Console.WriteLine($"Không tìm thấy TODO với Id {todoId}.");
            }
        }

        private async Task EditTodo()
        {
            Console.WriteLine("------ Chỉnh sửa TODO ------");
            Console.Write("Nhập Id của TODO cần chỉnh sửa: ");
            string todoId = Console.ReadLine();

            var todoToEdit = await todoManager.GetTodoById(todoId);

            if (todoToEdit != null)
            {
                Console.WriteLine($"Tiêu đề cũ: {todoToEdit.Title}");
                Console.Write("Nhập tiêu đề mới: ");
                string newTitle = Console.ReadLine();
                todoToEdit.Title = newTitle;

                Console.WriteLine($"Mô tả cũ: {todoToEdit.Description}");
                Console.Write("Nhập mô tả mới: ");
                string newDescription = Console.ReadLine();
                todoToEdit.Description = newDescription;

                await todoManager.UpdateTodo(todoToEdit);
                Console.WriteLine($"TODO có Id {todoId} đã được chỉnh sửa thành công.");
            }
            else
            {
                Console.WriteLine($"Không tìm thấy TODO với Id {todoId}.");
            }
        }
    }

}
