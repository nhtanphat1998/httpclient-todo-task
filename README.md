## Quản lý Công Việc với API và HttpClient

Bạn được giao nhiệm vụ viết chương trình dùng để quản lý công việc. bạn sẽ phải tạo ra một API server để lưu trữ dữ liệu. Các chức năng cơ bản quản lý công việc như hiển thị danh sách công việc, xoá công việc, đánh dấu công việc đã hoàn thành, chỉnh sửa công việc, thêm công việc mới và thoát ứng dụng.

### Các Chức Năng Cơ Bản:

1.  Hiển Thị Danh Sách Công Việc:

    -   Sử dụng HttpClient để gửi yêu cầu GET đến API server và hiển thị danh sách công việc.
2.  Xoá Công Việc:

    -   Cho phép người dùng xoá một công việc cụ thể.
    -   Sử dụng HttpClient để gửi yêu cầu DELETE đến API server.
3.  Đánh Dấu Công Việc Đã Hoàn Thành:

    -   Cho phép người dùng đánh dấu một công việc là đã hoàn thành.
    -   Sử dụng HttpClient để gửi yêu cầu PATCH đến API server để cập nhật trạng thái công việc.
4.  Chỉnh Sửa Công Việc:

    -   Cho phép người dùng chỉnh sửa thông tin của một công việc.
    -   Sử dụng HttpClient để gửi yêu cầu PUT đến API server để cập nhật thông tin công việc.
5.  Thêm Công Việc Mới:

    -   Cho phép người dùng thêm một công việc mới.
    -   Sử dụng HttpClient để gửi yêu cầu POST đến API server để tạo công việc mới.
6.  Thoát Ứng Dụng:

    -   Chương trình sẽ thoát sau khi hoàn thành tất cả các chức năng.

cấu trúc project như sau:
bạn hãy tự tạo ra cấu trúc project nếu gặp khó khăn thì có thể mở file `TaskManagement.sln` để có thể xem gợi ý.
![Project structure](project-structure.png)

### Hướng Dẫn Thực Hiện:

1.  Truy Cập API Server:

    -   Đăng ký và tạo một API server tại [mockapi.io](https://mockapi.io/).
2.  Xác Định Endpoints Cần Thiết của tasks:
    
    1.  Endpoint Hiển Thị Danh Sách Công Việc:

        -   Endpoint: `GET /tasks`
        -   Mô tả: Lấy danh sách tất cả công việc.
    2.  Endpoint Xoá Công Việc:

        -   Endpoint: `DELETE /tasks/{taskId}`
        -   Mô tả: Xoá công việc có ID là `{taskId}`.
    3.  Endpoint Đánh Dấu Công Việc Đã Hoàn Thành:

        -   Endpoint: `PATCH /tasks/{taskId}`
        -   Mô tả: Cập nhật trạng thái của công việc có ID là `{taskId}` thành đã hoàn thành.
    4.  Endpoint Chỉnh Sửa Công Việc:

        -   Endpoint: `PUT /tasks/{taskId}`
        -   Mô tả: Chỉnh sửa thông tin của công việc có ID là `{taskId}`.
    5.  Endpoint Thêm Công Việc Mới:

    -   Endpoint: `POST /tasks`
    -   Mô tả: Tạo một công việc mới.
3.  Sử Dụng HttpClient:

    -   Sử dụng `HttpClient` để thực hiện các yêu cầu HTTP đến API server.
    -   Sử dụng các phương thức không đồng bộ để đảm bảo ứng dụng chạy mượt mà.

### Bài Tập Nâng Cao:

1.  Chức Năng Đăng Ký và Đăng Nhập Người Dùng:

    -   Tạo một cổng endpoint cho người dùng để đăng ký và đăng nhập trên API server.
    -   Sử dụng `HttpClient` để gửi yêu cầu POST đến API server để quản lý người dùng.
2.  Quyền Truy Cập:

    -   Có hai loại người dùng: người dùng thông thường (user) và quản trị viên (admin).
    -   Người dùng thông thường chỉ có thể quản lý các công việc mà họ tạo ra.
    -   Quản trị viên có quyền xem và can thiệp vào tất cả các công việc được tạo ra từ mọi người.
3.  Thêm Thuộc Tính `createdBy` Cho TodoTask:

    -   Thêm thuộc tính `createdBy` vào class `TodoTask` để xác định người tạo công việc.
    -   Tạo class `User` để xác định người tạo. class này sẽ có các thuộc tính cơ bản như: Username, Password, DateOfBirth, FullName, Address, Role(`user hoặc admin`)
4.  Sử Dụng Filter Để Lọc Dữ Liệu:

    -   Sử dụng filter để lọc dữ liệu https://github.com/mockapi-io/docs/wiki/Code-examples#filtering
    -   Chỉ hiển thị các công việc phù hợp với quyền của người dùng.
    -   Xác định quyền(role của từng user) và cho phép truy cập dữ liệu phù hợp.

